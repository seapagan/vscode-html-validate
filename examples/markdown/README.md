# Markdown example

This example features HTML in Markdown.

## Vanilla HTML

```html
<p>Lorem ipsum</i>
```

## Vue.js

```vue
<template>
  <button :type="type">
    <a href="#">Lorem ipsum</a>
  </button>
</template>
```
