{
  "name": "vscode-html-validate",
  "displayName": "HTML-validate",
  "version": "2.6.0",
  "description": "vscode extension for html-validate",
  "categories": [
    "Programming Languages",
    "Linters"
  ],
  "keywords": [
    "html-validate",
    "multi-root ready"
  ],
  "homepage": "https://html-validate.org",
  "bugs": {
    "url": "https://gitlab.com/html-validate/vscode-html-validate/issues/new"
  },
  "repository": {
    "type": "git",
    "url": "https://gitlab.com/html-validate/vscode-html-validate.git"
  },
  "license": "MIT",
  "author": "David Sveningsson <ext@sidvind.com>",
  "publisher": "html-validate",
  "main": "./client/out/extension",
  "files": [
    "client/out",
    "server/out"
  ],
  "workspaces": [
    "client",
    "server",
    "examples/*"
  ],
  "scripts": {
    "build": "tsc -b",
    "postbuild": "npm run schema",
    "bundle": "run-s clean bundle:* schema",
    "bundle:client": "esbuild --bundle --external:vscode --format=cjs --platform=node --outfile=client/out/extension.js client/src/extension.ts",
    "bundle:server": "esbuild --bundle --external:vscode --format=cjs --platform=node --outfile=server/out/server.js server/src/server.ts",
    "clean": "rm -rf client/out server/out",
    "eslint": "eslint .",
    "eslint:fix": "eslint --fix .",
    "prepare": "husky install",
    "prettier:check": "prettier --check .",
    "prettier:write": "prettier --write .",
    "schema": "cp -R node_modules/html-validate/dist/schema .",
    "pretest": "npm run build",
    "test": "node ./client/out/test/runTest.js",
    "vscode:prepublish": "npm run bundle",
    "watch": "tsc -b -w"
  },
  "contributes": {
    "configuration": {
      "type": "object",
      "title": "HTML-Validate",
      "properties": {
        "html-validate.enable": {
          "scope": "resource",
          "type": "boolean",
          "default": true,
          "description": "Controls whether html-validate is enabled or not."
        },
        "html-validate.trace.server": {
          "scope": "window",
          "type": "string",
          "enum": [
            "off",
            "messages",
            "verbose"
          ],
          "default": "off",
          "description": "Traces the communication between VS Code and the language server."
        },
        "html-validate.validate": {
          "scope": "resource",
          "type": "array",
          "items": {
            "type": "string"
          },
          "default": [
            "html",
            "javascript",
            "markdown",
            "vue",
            "vue-html"
          ],
          "description": "An array of language ids which should be validated by HTML-Validate"
        }
      }
    },
    "jsonValidation": [
      {
        "fileMatch": ".htmlvalidate.json",
        "url": "./schema/config.json"
      },
      {
        "fileMatch": "elements.json",
        "url": "./schema/elements.json"
      }
    ]
  },
  "activationEvents": [
    "onLanguage:html",
    "workspaceContains:**/.htmlvalidate.{js,json}"
  ],
  "commitlint": {
    "extends": "@html-validate"
  },
  "lint-staged": {
    "*.{ts,js,json,md,scss}": "prettier --write"
  },
  "prettier": "@html-validate/prettier-config",
  "release": {
    "plugins": [
      "@semantic-release/commit-analyzer",
      "@semantic-release/release-notes-generator",
      [
        "semantic-release-vsce",
        {
          "packageVsix": "html-validate.vsix"
        }
      ],
      [
        "@semantic-release/gitlab",
        {
          "assets": [
            {
              "path": "html-validate.vsix",
              "label": "Package"
            }
          ]
        }
      ],
      [
        "@semantic-release/changelog",
        {
          "changelogTitle": "# vscode-html-validate changelog"
        }
      ],
      [
        "@semantic-release/exec",
        {
          "prepareCmd": "npm run prettier:write"
        }
      ],
      [
        "@semantic-release/git",
        {
          "message": "chore(release): ${nextRelease.version}\n\n${nextRelease.notes}"
        }
      ]
    ]
  },
  "devDependencies": {
    "@html-validate/commitlint-config": "3.0.11",
    "@html-validate/eslint-config": "5.5.23",
    "@html-validate/eslint-config-typescript": "5.5.24",
    "@html-validate/prettier-config": "2.3.6",
    "@semantic-release/changelog": "6.0.2",
    "@semantic-release/exec": "6.0.3",
    "@semantic-release/git": "10.0.1",
    "@semantic-release/gitlab": "10.1.4",
    "@semantic-release/npm": "9.0.2",
    "@semantic-release/release-notes-generator": "10.0.3",
    "@types/chai": "4.3.4",
    "@types/glob": "8.0.1",
    "@types/mocha": "10.0.1",
    "@types/node": "11.15.54",
    "@types/semver": "7.3.13",
    "chai": "4.3.7",
    "esbuild": "0.17.10",
    "glob": "8.1.0",
    "html-validate": "7.13.2",
    "husky": "8.0.3",
    "lint-staged": "13.1.2",
    "merge-options": "3.0.4",
    "mocha": "10.2.0",
    "npm": "9.5.0",
    "npm-run-all": "4.1.5",
    "prettier": "2.8.4",
    "semantic-release": "20.1.0",
    "semantic-release-vsce": "5.5.6",
    "typescript": "4.9.5",
    "vsce": "2.15.0"
  },
  "engines": {
    "node": ">= 12.0",
    "vscode": "^1.52.0"
  },
  "icon": "assets/icon.png",
  "renovate": {
    "extends": [
      "gitlab>html-validate/renovate-config"
    ],
    "ignorePaths": [
      "**/node_modules/**"
    ],
    "packageRules": [
      {
        "packageNames": [
          "vscode-languageclient",
          "vscode-languageserver"
        ],
        "groupName": "vscode-lsp",
        "groupSlug": "vscode-lsp"
      }
    ]
  }
}
