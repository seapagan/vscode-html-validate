import vscode, { Uri } from "vscode"; /* eslint-disable-line import/no-unresolved */

export function ruleDocumentation(url: string): void {
	if (!url) {
		return;
	}
	vscode.env.openExternal(Uri.parse(url));
}
