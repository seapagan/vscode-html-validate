/* eslint-disable import/no-mutable-exports */

import path from "path";

/* eslint-disable-next-line import/no-unresolved */
import vscode, { Uri } from "vscode";

export async function activate(docUri: vscode.Uri): Promise<void> {
	const ext = vscode.extensions.getExtension("html-validate.vscode-html-validate");
	await ext.activate();
	try {
		const doc = await vscode.workspace.openTextDocument(docUri);
		await vscode.window.showTextDocument(doc);
		await sleep(10000); // Wait for server activation
	} catch (e) {
		/* eslint-disable-next-line no-console */
		console.error(e);
	}
}

async function sleep(ms: number): Promise<void> {
	return new Promise((resolve) => setTimeout(resolve, ms));
}

export function getDocUri(filename: string): Uri {
	const filePath = path.resolve(__dirname, "../../../examples", filename);
	return vscode.Uri.file(filePath);
}
