import {
	CodeActionParams,
	CodeAction,
	Diagnostic,
	Command,
	CodeActionKind,
} from "vscode-languageserver";
import { URI } from "vscode-uri";
import { HtmlValidate } from "html-validate";
import { ApiContext } from "./api-context";

const enum COMMAND {
	RULE_DOCUMENTATION = "html-validate.rule-documentation",
}

/**
 * Create code action to open rule documentation if a URL is available.
 */
function ruleDocumentationAction(
	htmlvalidate: HtmlValidate,
	diagnostic: Diagnostic
): CodeAction | null {
	/* only handle diagnostic messages for this extension */
	if (diagnostic.source !== "html-validate" || !diagnostic.code) {
		return null;
	}

	/* fetch rule documentation */
	const ruleId = diagnostic.code as string;
	const documentation = htmlvalidate.getRuleDocumentation(ruleId);
	if (!documentation) {
		return null;
	}

	const title = `Show documentation for ${ruleId}`;
	const command = Command.create(title, COMMAND.RULE_DOCUMENTATION, documentation.url);

	return CodeAction.create(title, command, CodeActionKind.QuickFix);
}

/**
 * Create code actions for diagnostic messages.
 */
export async function onCodeAction(
	this: ApiContext,
	params: CodeActionParams
): Promise<CodeAction[]> {
	const { context } = params;
	const resource = params.textDocument.uri;
	const uri = URI.parse(resource);

	const htmlvalidate = await this.getDocumentValidator(uri.path);
	if (!htmlvalidate) {
		return [];
	}

	const actions: CodeAction[] = [];

	for (const diagnostic of context.diagnostics) {
		try {
			const action = ruleDocumentationAction(htmlvalidate, diagnostic);
			if (action) {
				actions.push(action);
			}
		} catch (error) {
			this.trace(error);
		}
	}

	return actions;
}
